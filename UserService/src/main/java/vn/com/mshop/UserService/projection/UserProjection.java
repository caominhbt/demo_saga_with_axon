package vn.com.mshop.UserService.projection;

import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Component;
import vn.com.mshop.CommonService.models.CardDetails;
import vn.com.mshop.CommonService.models.User;
import vn.com.mshop.CommonService.queries.GetUserPaymentDetailsQuery;

@Component
public class UserProjection {

    @QueryHandler
    public User getUserPaymentDetail(GetUserPaymentDetailsQuery query) {
        CardDetails cardDetails = CardDetails.builder()
                .name("Nguyen Cao Minh")
                .cvv(121)
                .validUtilMonth(12)
                .validUtilYear(2021)
                .cardNumber("1232123211")
                .build();
        return User.builder()
                .cardDetails(cardDetails)
                .userId(query.getUserId())
                .lastName("Nguyen")
                .firstName("Minh")
                .build();
    }
}
