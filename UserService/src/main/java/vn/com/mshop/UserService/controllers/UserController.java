package vn.com.mshop.UserService.controllers;

import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import vn.com.mshop.CommonService.models.User;
import vn.com.mshop.CommonService.queries.GetUserPaymentDetailsQuery;

@Controller
@RequestMapping("/users")
public class UserController {
    @Autowired
    QueryGateway queryGateway;

    @GetMapping("{userId}")
    public User getUserPaymentDetails(@PathVariable("userId") String userId) {
        GetUserPaymentDetailsQuery getUserPaymentDetailsQuery = new GetUserPaymentDetailsQuery(userId);
        return queryGateway.query(getUserPaymentDetailsQuery, ResponseTypes.instanceOf(User.class)).join();
    }
}
