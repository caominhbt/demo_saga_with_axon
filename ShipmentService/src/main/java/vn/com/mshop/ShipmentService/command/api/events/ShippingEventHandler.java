package vn.com.mshop.ShipmentService.command.api.events;

import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.com.mshop.CommonService.events.OrderShippedEvent;
import vn.com.mshop.CommonService.events.ShippingCancelledEvent;
import vn.com.mshop.ShipmentService.command.api.data.Shipping;
import vn.com.mshop.ShipmentService.command.api.data.ShippingRepository;

@Component
public class ShippingEventHandler {

    @Autowired
    private ShippingRepository shippingRepository;

    @EventHandler
    public void on (OrderShippedEvent event){
        Shipping shipping = new Shipping();
        BeanUtils.copyProperties(event, shipping);
        shippingRepository.save(shipping);
    }

    public void on(ShippingCancelledEvent event) {
        Shipping shipping = shippingRepository.findById(event.getShippingId()).get();
        shipping.setShippingStatus(event.getShippingStatus());
        shippingRepository.save(shipping);
    }
}
