package vn.com.mshop.ShipmentService.command.api.aggregate;

import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.TargetAggregateIdentifier;
import org.axonframework.modelling.saga.SagaEventHandler;
import org.axonframework.spring.stereotype.Aggregate;
import org.springframework.beans.BeanUtils;
import vn.com.mshop.CommonService.commands.CancelShippingCommand;
import vn.com.mshop.CommonService.commands.ShippingOrderCommand;
import vn.com.mshop.CommonService.events.OrderShippedEvent;
import vn.com.mshop.CommonService.events.ShippingCancelledEvent;

@Aggregate
public class ShippingAggregate {

    @AggregateIdentifier
    private String shippingId;
    private String orderId;
    private String shippingStatus;

    public ShippingAggregate() {

    }

    public ShippingAggregate(ShippingOrderCommand shippingOrderCommand) {
        //validate shippingOrderCommand
        //publish ShippedOrderCommand
        OrderShippedEvent orderShippedEvent = OrderShippedEvent.builder()
                .shippingId(shippingOrderCommand.getShippingId())
                .orderId(shippingOrderCommand.getOrderId())
                .shippingStatus("COMPLETED")
                .build();
        AggregateLifecycle.apply(orderShippedEvent);
    }

   @EventSourcingHandler
    public void on(OrderShippedEvent event){
        this.shippingId = event.getShippingId();
        this.orderId = event.getOrderId();
        this.shippingStatus = event.getShippingStatus();
    }

    @CommandHandler
    public void handle(CancelShippingCommand cancelShippingCommand) {
        ShippingCancelledEvent event = new ShippingCancelledEvent();
        BeanUtils.copyProperties(cancelShippingCommand,event);
        AggregateLifecycle.apply(event);
    }

    @EventSourcingHandler
    public void on(ShippingCancelledEvent event) {
        this.shippingId = event.getShippingId();
        this.orderId = event.getOrderId();
        this.shippingStatus = event.getShippingStatus();
    }
}
