package vn.com.mshop.ShipmentService.command.api.data;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name ="Shipping")
public class Shipping {

    @Id
    private String shippingId;

    private String orderId;

    private String shippingStatus;
}
