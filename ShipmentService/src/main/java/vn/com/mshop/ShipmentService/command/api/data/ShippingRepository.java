package vn.com.mshop.ShipmentService.command.api.data;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ShippingRepository extends JpaRepository<Shipping, String> {
}
