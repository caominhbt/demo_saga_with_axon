package vn.com.mshop.CommonService.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CardDetails {
    private String name;
    private String cardNumber;
    private Integer validUtilMonth;
    private Integer validUtilYear;
    private Integer cvv;
}
