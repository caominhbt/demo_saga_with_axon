package vn.com.mshop.CommonService.commands;

import lombok.Value;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Value
public class CancelShippingCommand {

    @TargetAggregateIdentifier
    private String shippingId;
    private String orderId;
    private String shippingStatus = "CANCELLED";
}
