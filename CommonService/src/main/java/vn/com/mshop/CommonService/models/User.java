package vn.com.mshop.CommonService.models;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User {

    private String userId;
    private String lastName;
    private String firstName;
    private CardDetails cardDetails;
}
