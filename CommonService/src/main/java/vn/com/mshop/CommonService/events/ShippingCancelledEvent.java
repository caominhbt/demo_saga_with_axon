package vn.com.mshop.CommonService.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShippingCancelledEvent {

    private String shippingId;
    private String orderId;
    private String shippingStatus;
}
