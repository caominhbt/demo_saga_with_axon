package vn.com.mshop.CommonService.commands;


import lombok.Builder;
import lombok.Data;
import org.axonframework.modelling.command.TargetAggregateIdentifier;

@Data
@Builder
public class ShippingOrderCommand {

    @TargetAggregateIdentifier
    private String shippingId;
    private String orderId;

}
