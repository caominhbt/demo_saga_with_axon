package vn.com.mshop.PaymentService.command.api.event;

import org.axonframework.eventhandling.EventHandler;
import org.springframework.stereotype.Component;
import vn.com.mshop.CommonService.events.PaymentCancelledEvent;
import vn.com.mshop.CommonService.events.PaymentProcessedEvent;
import vn.com.mshop.PaymentService.command.api.data.Payment;
import vn.com.mshop.PaymentService.command.api.data.PaymentRepository;

import java.util.Date;

@Component
public class PaymentsEventHandler {

    private PaymentRepository paymentRepository;

    @EventHandler
    public void on(PaymentProcessedEvent event) {
        Payment payment = Payment.builder()
                .paymentId(event.getPaymentId())
                .orderId(event.getOrderId())
                .paymentStatus("COMPLETED")
                .timestamp(new Date())
                .build();
        paymentRepository.save(payment);
    }

    @EventHandler
    public void on(PaymentCancelledEvent event) {
        Payment payment = paymentRepository.findById(event.getPaymentId()).get();
        payment.setPaymentStatus(event.getPaymentStatus());
        paymentRepository.save(payment);
    }
}
