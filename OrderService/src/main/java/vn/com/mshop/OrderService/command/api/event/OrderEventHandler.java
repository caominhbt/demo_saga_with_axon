package vn.com.mshop.OrderService.command.api.event;

import org.axonframework.eventhandling.EventHandler;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import vn.com.mshop.CommonService.events.OrderCancelledEvent;
import vn.com.mshop.CommonService.events.OrderCompletedEvent;
import vn.com.mshop.OrderService.command.api.persistent.Order;
import vn.com.mshop.OrderService.command.api.persistent.OrderRepository;

@Component
public class OrderEventHandler {

    @Autowired
    private OrderRepository orderRepository;

    @EventHandler
    public void on(OrderCreatedEvent event) {
        Order order = new Order();
        BeanUtils.copyProperties(event, order);

        orderRepository.save(order);
    }

    @EventHandler
    public void on (OrderCompletedEvent event){
        Order order = orderRepository.findById(event.getOrderId()).get();
        order.setOrderStatus(event.getOrderStatus());
        orderRepository.save(order);
    }

    @EventHandler
    public void on(OrderCancelledEvent orderCancelledEvent) {
        Order order = orderRepository.findById(orderCancelledEvent.getOrderId()).get();
        order.setOrderStatus(orderCancelledEvent.getOrderStatus());
        orderRepository.save(order);
    }
}
