package vn.com.mshop.OrderService.command.api.persistent;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<Order, String> {
}
