package vn.com.mshop.OrderService.command.api.event;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
public class OrderCreatedEvent {

    private String orderId;
    private String productId;
    private String userId;
    private String addressId;
    private Integer quantity;
    private String orderStatus;

}
